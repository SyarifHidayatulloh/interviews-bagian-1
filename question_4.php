
<?php

$input = 64;
table($input);

function table($int)
{
    // Pola: Setiap kelipatan 3 dan 4 putih, selain itu hitam
    echo '<table cellspacing="0" cellpadding="0" style="">';
    
    for($i = 1; $i <= $int; $i++) {
        // cek apakah index sama dengan kelipatan 3 atau kelipatan 4
        $tableCell = ($i % 3 != 0 && $i % 4 != 0) ? '<td style="padding-left: 1rem; padding-right: 1rem; padding-top: .75rem; padding-bottom: .75rem; background-color: black; color: white;">'.$i.'</td>' : '<td style="padding-left: 1rem; padding-right: 1rem; padding-top: .75rem; padding-bottom: .75rem;">'.$i.'</td>'; 
        
        // cek apakah index sama dengan input
        $tableRow = $i == $int ? "</tr>" : "</tr><tr>";
        
        echo $i % 8 == 0 ? $tableCell.$tableRow : $tableCell;
    }
    
    echo '</table>';
}