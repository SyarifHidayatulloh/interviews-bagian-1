<?php

$input = "DFHKNQ";
encrypt($input);

function encrypt($text)
{
    // init list dari a sampai z
    $arrAlphabet = range("A", "Z");
    $arrText = str_split($text);
    $tempArr = [];

    foreach ($arrText as $key => $value) 
    {
        $idx = $key + 1;
        $idxAlphabet = array_search($value, $arrAlphabet);

        // Pola: jika index ganjil maka +1 dan genap sebaliknya
        $letter = $idx % 2 == 0 ? $arrAlphabet[$idxAlphabet - $idx] : $arrAlphabet[$idxAlphabet + $idx];

        array_push($tempArr, $letter);
    }

    echo join("", $tempArr);
}