<?php

$input = "72 65 73 78 75 74 90 81 87 65 55 69 72 78 79 91 100 40 67 77 86";
cari($input);

function cari($nilai)
{
    $arr = explode(" ", $nilai);

    // Nilai rata-rata
    $average = array_sum($arr)/count($arr);
    
    // 7 nilai tertinggi
    rsort($arr);
    $max = join(" ",array_slice($arr, 0, 6));
    
    // 7 nilai terendah
    sort($arr);
    $min = join(" ", array_slice($arr, 0, 6));
    
    echo sprintf("Nilai rata-rata: %s\n7 nilai tertinggi: %s\n7 nilai terendah: %s\n", $average, $max, $min);
}