<?php

$input = "Jakarta adalah ibukota negara Republik Indonesia";
cari($input);

function cari($text) {
    ngram($text, "Unigram", 1);
    ngram($text, "Bigram", 2);
    ngram($text, "Trigram", 3);
}

// Cari ngram sesuai n
function ngram($text, $type, $n) 
{
    // Init array
    $temp = [];
    $arr_final = [];

    // List kata dari kalimat
    $arr = explode(" ", $text);

    foreach ($arr as $value)
    {
        // Push setiap kata ke temporary array
        array_push($temp, $value);

        if (count($temp) == $n)
        { 
            // Push temporary array ke array baru jika jumlah kata sama dengan n
            array_push($arr_final, join(" ", $temp));
            $temp = [];
        }
    }

    echo sprintf("%s : %s\n", $type, join(" ,", $arr_final));
}