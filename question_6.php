<?php

$arr = [
    ['f', 'g', 'h', 'i'],
    ['j', 'k', 'p', 'q'],
    ['r', 's', 't', 'u']
];

cari($arr, 'fghi'); // true 
cari($arr, 'fghp'); // true
cari($arr, 'fjrstp'); // true 
cari($arr, 'fghq'); // false
cari($arr, 'fst'); // false
cari($arr, 'pqr'); // false
cari($arr, 'fghh'); // false

function cari($arr, $word) {
    $arrWord = str_split($word);
    $arrStatus = [];

    foreach ($arrWord as $key => $value) {
        if($key + 1 != count($arrWord)){
            $nextLetter = $arrWord[$key+1];

            foreach ($arr as $key2 => $value2) {
                $idx = array_search($value, $value2);

                if($idx != ""){
                    // get huruf disekitar
                    $left = isset($arr[$key2][$idx-1]) ? $arr[$key2][$idx-1] : "..";
                    $top = isset($arr[$key2-1][$idx]) ? $arr[$key2-1][$idx] : "..";
                    $right = isset($arr[$key2][$idx+1]) ? $arr[$key2][$idx+1] : "..";
                    $bottom = isset($arr[$key2+1][$idx]) ? $arr[$key2+1][$idx] : "..";
        
                    // check apakah huruf setelahnya ada yang sama dengan sekitar atau tidak
                    if(!in_array($nextLetter, [$left, $top, $right, $bottom])){
                        echo "false\n";
                        return false;
                    }
                }
            }
        }
    }

    echo "true\n";
    return true;
}